cd site/blog
python compile.py
cd ../..
python genstatic.py
curl -X POST -s --data-urlencode 'input@site/dllu.css' http://cssminifier.com/raw > site/dllu.min.css
cp site/humans.txt readme.md
cp main.py ~/website/main.py
cp app.yaml ~/website/app.yaml
mkdir ~/website_temp
rsync -r --exclude=\.* s/ ~/website_temp
rsync -r --exclude=\.* projects/ ~/website_temp
rsync -r --exclude=\.* site/ ~/website_temp
rsync -r --delete ~/website_temp/ ~/website/static
rm -r ~/website_temp
# dev_appserver.py ~/website
appcfg.py update ~/website
